# starlight-upload

## Getting started

- [ ] Create helm chart for nginx
- [ ] Set up ingress for static assets
- [ ] Mount nfs volume with assets into pod
- [ ] Remap nginx over asset mount